import { CognitoJwtVerifier } from "aws-jwt-verify";
import { APIGatewayRequestAuthorizerEventV2 } from 'aws-lambda';

import {allow, disallow} from './policy'

const verifier = CognitoJwtVerifier.create({
  userPoolId: process.env.USER_POOL_ID,
  tokenUse: "access",
  clientId: process.env.USER_POOL_CLIENT_ID,
});

export const handler = (event: APIGatewayRequestAuthorizerEventV2) => {
  const key = event.headers.Authorization
  if (!key) {
    console.log('Key not present in request')
    return disallow(event)
  }
  const split = key.split(' ')
  if (split.length !== 2 || split[0].toLocaleLowerCase() !== 'bearer') {
    console.log('Invalid key format')
    return disallow(event)
  }
  return verifier.verify(split[1])
    .then(payload => {
      console.log('JWT valid for user ' + payload.username)
      return allow(event, payload)
    })
    .catch((err) => {
      console.log('JWT Not valid: ' + err)
      return disallow(event)
    })
}