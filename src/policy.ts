export const allow = (event, context) => {
  return {
    "principalId": "user", // The principal user identification associated with the token sent by the client.
    "policyDocument": {
      "Version": "2012-10-17",
      "Statement": [{
        "Action": "execute-api:Invoke",
        "Effect": "Allow",
        "Resource": event.routeArn
      }]
    },
    context
  };
}

export const disallow = (event) => {
  return {
    "principalId": "unauthorized", // The principal user identification associated with the token sent by the client.
    "policyDocument": {
      "Version": "2012-10-17",
      "Statement": [{
        "Action": "execute-api:Invoke",
        "Effect": "Deny",
        "Resource": event.routeArn
      }]
    },
    "context": {
    }
  };
}