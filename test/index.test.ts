import dotenv from 'dotenv'
dotenv.config()

import * as lambda from '../src'
import * as testHelper from './helper'
describe('So I can run it locally.', () => {
  lambda.handler(testHelper.getRequest({
    Authorization: 'Bearer ' + 'some token'
  }))
})
